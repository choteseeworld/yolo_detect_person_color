import numpy as np
import time
import cv2
from darkflow.net.build import TFNet
import matplotlib.pyplot as plt


def detectColor(frame):
    hsv = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)

    # defining the range of Orange color
    # <-  change color lower for detect color
    orange_lower = np.array([0, 150, 150], np.uint8)
    # <-  change color upper for detect color
    orange_upper = np.array([46, 242, 255], np.uint8)

    # finding the range yellow colour in the image
    orange = cv2.inRange(hsv, orange_lower, orange_upper)

    # Morphological transformation, Dilation
    kernal = np.ones((5, 5), "uint8")
    blue = cv2.dilate(orange, kernal)
    # res = cv2.bitwise_and(frame, frame, mask=orange)

    # Tracking Colour (Orange)
    (_, contours, hierarchy) = cv2.findContours(
        orange, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)

    for pic, contour in enumerate(contours):
        area = cv2.contourArea(contour)
        if(area > 300):
            x, y, w, h = cv2.boundingRect(contour)
            img = cv2.rectangle(frame, (x, y), (x+w, y+h), (51, 153, 255), 2)
            cv2.putText(frame, "orange", (x+5, y + 5),
                        cv2.FONT_HERSHEY_SIMPLEX, 1, (51, 153, 255), 2)


options = {
    'model': 'cfg/yolo.cfg',
    'load': 'bin/yolov2.weights',
    'threshold': 0.3

}
tfnet = TFNet(options)

cap = cv2.VideoCapture(0)

colors = [tuple(255 * np.random.rand(3)) for i in range(5)]

while(cap.isOpened()):
    stime = time.time()
    ret, frame = cap.read()
    results = tfnet.return_predict(frame)
    if ret:
        for color, result in zip(colors, results):
            if result['label'] == 'person':
                tl = (result['topleft']['x'], result['topleft']['y'])
                br = (result['bottomright']['x'], result['bottomright']['y'])

                label = result['label']
                frame = cv2.rectangle(frame, tl, br, color, 7)
                frame = cv2.putText(
                    frame, label, tl, cv2.FONT_HERSHEY_TRIPLEX, 1, (0, 0, 0), 2)

                personImg = frame[result['topleft']['y']:result['bottomright']
                                  ['y'], result['topleft']['x']:result['bottomright']['x']]

                detectColor(personImg)
                # for i in range(len(arr_pos)):
                #     x = arr_pos[i].x
                #     y = arr_pos[i].y
                #     w = arr_pos[i].w
                #     h = arr_pos[i].h

                #     cv2.rectangle(frame, frame, (x, y), (x+w, y+h), (51, 153, 255), 2)
                #     cv2.putText(frame, "orange", (x+5, y+5), cv2.FONT_HERSHEY_SIMPLEX, 1, (51, 153, 255), 2)

        cv2.imshow('frame', frame)
        print('FPS {:1f}'.format(1/(time.time() - stime)))
        if cv2.waitKey(1) & 0xFF == ord('q'):
            break
    else:
        break

cap.release()
cv2.destroyAllWindows()
